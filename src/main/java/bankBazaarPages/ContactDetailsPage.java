package bankBazaarPages;

import org.openqa.selenium.WebElement;

import wdMethods.SeMethods;

public class ContactDetailsPage extends SeMethods {

	public ContactDetailsPage enterFirstName(String data)
	{
		WebElement eleFirstName = locateElement("name", "firstName");
		type(eleFirstName, data);
		return this;
	}
	
	public MFListPage clickViewMF()
	{
		WebElement eleViewMF = locateElement("linktext", "View Mutual Funds");
		click(eleViewMF);
		try {
			Thread.sleep(7000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new MFListPage();
	}
}
