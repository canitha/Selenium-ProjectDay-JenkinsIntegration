package bankBazaarPages;

import java.util.List;

import org.openqa.selenium.WebElement;

import wdMethods.SeMethods;

public class MFListPage extends SeMethods {

	public MFListPage getMFSchemaNameandPrice()
	{	
		
		List<WebElement> listOfMFNames = locateElements("class", "js-offer-name");
		List<WebElement> listOfMFPrice = locateElements("xpath", "//div[contains(@class,'offer-section-column')]/span[@class=\"js-title\"]");
		for (int i = 0; i < listOfMFNames.size(); i++) {
			System.out.println("Schema Name: "+getText(listOfMFNames.get(i)));
			System.out.println("Investment Price: "+getText(listOfMFPrice.get(i)));
			
		}		
		return this;
	}}
