package bankBazaarPages;

import org.openqa.selenium.WebElement;

import wdMethods.SeMethods;

public class BankActPage extends SeMethods{

	public ContactDetailsPage selectBank()
	{
		WebElement eleBankName = locateElement("xpath", "//span[text()=\"ICICI\"]//following-sibling::span/input");
		click(eleBankName);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ContactDetailsPage();
		
	}
}
