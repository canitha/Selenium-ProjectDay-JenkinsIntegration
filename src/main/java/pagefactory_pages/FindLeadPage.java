package pagefactory_pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import wdMethods.ProjectMethods;

public class FindLeadPage  extends ProjectMethods{

	public FindLeadPage()
	{
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath="(//input[@name='firstName'])[3]")
	WebElement elefirstName;
	@FindBy(xpath="(//input[@name='lastName'])[3]")
	WebElement elelastName;
	@FindBy(xpath="//button[text()='Find Leads']")
	WebElement eleFindBtn;
	@FindBy(xpath="(//div[@class='x-grid3-scroller']//a)[1]")
	WebElement eleFirstId;
	
	public FindLeadPage typeFindFirstName(String data) {
		type(elefirstName, data);
		return this;
	}
	
	public FindLeadPage typeFindLastName(String data) {
		type(elelastName, data);
		return this;
	}

	public FindLeadPage clickFindLeads() throws InterruptedException {
		click(eleFindBtn);
		Thread.sleep(3000);
		return this;
	}

	public ViewLeadPage clickFirstLeads() {
		clickWithNoSnap(eleFirstId);
		return new ViewLeadPage();
	}	
}
