package pagefactory_pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class WindowFindLeadPage extends ProjectMethods{

	public WindowFindLeadPage() {
		PageFactory.initElements(driver, this);

	}
	@FindBy(name="firstName")
	WebElement eleFirstName;
	@FindBy(xpath="//button[text()='Find Leads']")
	WebElement eleFindLeads;
	@FindBy(xpath="//a[@class='linktext']")
	WebElement eleFirstId;
	/*@FindBy(name="firstName")
	WebElement eleFirstName2;
	@FindBy(xpath="//button[text()='Find Leads']")
	WebElement eleFindLeads1;	
	@FindBy(xpath= "//a[@class='linktext']")
	WebElement eleSecondId;*/

	public WindowFindLeadPage typeFirstName(String data)
	{
		type(eleFirstName, data);
		return this;
	}
	public WindowFindLeadPage clickFindLeadBtn()
	{
		click(eleFindLeads);
		return this;
	}

	public MergeLeadPage clickFirstId()
	{
		clickWithNoSnap(eleFirstId);
		switchToWindow(0);
		return new MergeLeadPage();
	}
}

