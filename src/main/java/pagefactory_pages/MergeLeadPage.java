package pagefactory_pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import pages.FindLeadPage;
import wdMethods.ProjectMethods;

public class MergeLeadPage extends ProjectMethods {

	public MergeLeadPage()
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(xpath="(//img[@alt='Lookup'])[1]")
	WebElement eleFirstLookup;
	@FindBy(xpath= "(//img[@alt='Lookup'])[2]")
	WebElement eleSecondLookup;
	@FindBy(xpath="//a[@class='buttonDangerous']")
	WebElement eleMergebutton;	

	public WindowFindLeadPage clickFirstLookup()
	{	
		click(eleFirstLookup);
		switchToWindow(1);
		return new WindowFindLeadPage();
	}

	/*public WindowFindLeadPage clickSecondLookup()
	{
		click(eleSecondLookup);
		//switchToWindow(1);
		return new WindowFindLeadPage();

		public ViewLeadPage clickMergeButton()
		{
			click(eleMergebutton);
			acceptAlert();
			return ViewLeadPage;
		}

	}
*/
}
