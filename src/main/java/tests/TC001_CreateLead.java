package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.MyHomePage;
import wdMethods.ProjectMethods;

public class TC001_CreateLead extends ProjectMethods {
	
	@BeforeClass
	public void setData() {
		testCaseName = "TC001_CreateLead";
		testCaseDescription ="Create a lead";
		category = "Smoke";
		author= "Babu";
		dataSheetName="CreateLeadData";
	}
	@Test(dataProvider="fetchData")
	public  void createLead(String compName, String firstName, String lastName, String dataSource, String marketId)   {
		new MyHomePage()
		.clickLeads()
		.clickCreateLead()
		.typeCompanyName(compName)
		.typeFirstName(firstName)
		.typeLastName(lastName)
		.selectSource(dataSource)
		.selectMarketId(marketId)		
		.clickCreateLead();	
	}
	

}
