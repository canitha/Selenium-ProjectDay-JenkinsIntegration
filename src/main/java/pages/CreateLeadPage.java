package pages;

import org.openqa.selenium.WebElement;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{
	@And("enter Company Name as (.*)")
	public CreateLeadPage typeCompanyName(String data) {
		WebElement eleCompanyName = locateElement("id", "createLeadForm_companyName");
		type(eleCompanyName, data);
		return this;
	}
	@And("enter First Name as (.*)")
	public CreateLeadPage typeFirstName	(String data) {
		WebElement eleFirstName = locateElement("id", "createLeadForm_firstName");
		type(eleFirstName, data);
		return this;
	}
	@And("enter Last Name as(.*)")
	public CreateLeadPage typeLastName	(String data) {
		WebElement eleLastName = locateElement("id", "createLeadForm_lastName");
		type(eleLastName, data);
		return this;
	}
	
	public CreateLeadPage selectSource(String data) {
		WebElement eleddSource= locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(eleddSource, data);
		return this;
	}
	public CreateLeadPage selectMarketId(String data) {
		WebElement eleddMkt= locateElement("id", "createLeadForm_marketingCampaignId");
		selectDropDownUsingText(eleddMkt, data);
		return this;
	}
	@When("click on Create Lead button")
	public ViewLeadPage clickCreateLead() {
		WebElement eleCreateLead= locateElement("class", "smallSubmit");
		click(eleCreateLead);
		return new ViewLeadPage(); 
	}
	
}









