package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import pagefactory_pages.MergeLeadPage;
import wdMethods.ProjectMethods;

public class MyLeadsPage extends ProjectMethods{
	
	public MyLeadsPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(linkText="Create Lead")
	WebElement eleCreateLead;
	@FindBy(linkText="Find Leads")
	WebElement eleFindLead;
	@FindBy(linkText="Merge Leads")
	WebElement eleMergeLead;
	
	@And("click on Create Lead Link from Shortcuts")
	public CreateLeadPage clickCreateLead() {
		click(eleCreateLead);
		return new CreateLeadPage();
	}
	
	public FindLeadPage clickFindLead(){
		click(eleFindLead);
		return new FindLeadPage();
	}
	
	public MergeLeadPage clickMergeLead() {
		click(eleMergeLead);
		return new MergeLeadPage();
	}
	
}









